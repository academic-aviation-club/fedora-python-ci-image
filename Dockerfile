FROM fedora:34

RUN dnf install -y \
    gcc git findutils \
    poetry python-devel tox \
    protobuf-compiler clang-tools-extra docker-compose ruby npm \
    zlib-devel bzip2-devel openssl-devel readline-devel sqlite-devel \
    && dnf clean all

RUN gem install mdl:0.11.0  # Markdown lint

RUN curl https://pyenv.run | bash
# hadolint ignore=SC2016
RUN echo 'PATH="$HOME/.pyenv/bin:$PATH"' > ~/.bashrc

# RUN ~/.pyenv/bin/pyenv install 3.5.10 || ~/.pyenv/bin/pyenv install 3.5.10
RUN ~/.pyenv/bin/pyenv install 3.6.15 || ~/.pyenv/bin/pyenv install 3.6.15
RUN ~/.pyenv/bin/pyenv install 3.7.12
RUN ~/.pyenv/bin/pyenv install 3.8.12
RUN ~/.pyenv/bin/pyenv install 3.9.7
RUN ~/.pyenv/bin/pyenv install 3.10.0

